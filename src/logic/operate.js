import Big from "big.js";

const operate = (numberOne, numberTwo, operation) => {

    const [first, second] = [numberOne, numberTwo].map(str => new Big(str));

    const signMap = () => {
        switch(operation){
            case '+':
                return first.plus(second);
            case '-':
                return first.minus(second);
            case 'x':
                return first.times(second);
            case '/':
                return first.div(second);
        }
    };

    return signMap();
};

export default operate;
