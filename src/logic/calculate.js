import operate from "./operate";

const calculate = (calculatorData, buttonName) => {

    const dial = (dialNumber, calculatorData) => {
        calculatorData.next = calculatorData.next || '0';
        if (calculatorData.next === '0') calculatorData.next = '';
        if (!calculatorData.operation) calculatorData.total = '0';
        return {...calculatorData, next: calculatorData.next + dialNumber }
    };

    const performOperation = (calculatorData) => {
        return {...calculatorData, total: operate(calculatorData.total, calculatorData.next, calculatorData.operation).valueOf(), next: null}
    };

    const equal = (calculatorData) => {
        return {...performOperation(calculatorData), operation: null}
    }

    let next, total;
    ({ next, total } = calculatorData);
    next = next || '0';
    total = total || '0';

    let sanitized = {...calculatorData, next: next, total: total};

    const operationsMapping = (buttonName, calculatorData) => {

        switch(buttonName){
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                return dial(buttonName, calculatorData);
            case '+':
            case '-':
            case 'x':
            case '/':
                calculatorData.operation = buttonName;
                return performOperation(calculatorData);
            case '=':
                return performOperation(calculatorData);
        }
    };

    return operationsMapping(buttonName, sanitized);
};

export default calculate;

