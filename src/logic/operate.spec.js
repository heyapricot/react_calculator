import operate from './operate'
import Big from 'big.js'

describe ('operate()', () => {

    const [first, second] = ['2','3'];
    const [firstBig, secondBig] = [first, second].map(str => new Big(str));

    test('When operation === "+"', () => {
        expect(operate(first, second, '+')).toEqual(firstBig.plus(secondBig))
    });

    test('When operation === "-"', () => {
        expect(operate(first, second, '-')).toEqual(firstBig.minus(secondBig))
    });

    test('When operation === "x"', () => {
        expect(operate(first, second, 'x')).toEqual(firstBig.times(secondBig))
    });

    test('When operation === "/"', () => {
        expect(operate(first, second, '/')).toEqual(firstBig.div(secondBig))
    });
});
