import React from 'react';
import Display from './Display';
import ButtonPanel from './ButtonPanel';
import calculate from "../logic/calculate";

export default class App extends React.Component {
    constructor(props){
        super(props);
        this.style = {
            width: '700px',
        }

        this.state = {
            total: null,
            next: null,
            operation: null,
        }

    }

    handleClick = (buttonName) => {
        this.setState(calculate(this.state, buttonName));
    };

    render() {
        return (
            <div style={this.style}>
                <Display result={(this.state.next) ? this.state.next : this.state.total}/>
                <ButtonPanel onClick={this.handleClick}/>
            </div>
        )
    }
}

App.defaultProps = {
    total: 0,
};