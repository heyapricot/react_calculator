import React from 'react';

export default class Display extends React.Component {
    constructor(props){
        super(props);

        this.divStyle = {
            display: 'flex',
            backgroundColor: 'gray',
            height: '100px',
            color: 'white',
            fontWeight: 'bold',
            padding: '1em',
        }

        this.pStyle = {
            textAlign: 'right',
            width: '100%',
        }
    }


    render() {
        return (
            <div style={this.divStyle}>
                <p style={this.pStyle}>{this.props.result}</p>
            </div>
        )
    }
}

