import React from 'react';
import Button from "./Button";

export default class Display extends React.Component {
    constructor(props){
        super(props);

        this.handleClick = this.handleClick.bind(this);

        this.style = {
            display: 'flex',
            flexWrap: 'wrap',
        };

        this.rowStyle = {
            display: 'flex',
            width: '100%',
            height: 100,
        }


    }

    handleClick = (buttonName) => { this.props.onClick(buttonName) };

    render() {
        return (
            <div style={this.style}>
                <div style={this.rowStyle}>
                    <Button onClick={this.handleClick} color='lightgray' name="AC" />
                    <Button onClick={this.handleClick} color='lightgray' name="+/-" />
                    <Button onClick={this.handleClick} color='lightgray' name="%" />
                    <Button onClick={this.handleClick} name="/" />
                </div>
                <div style={this.rowStyle}>
                    <Button onClick={this.handleClick} color='lightgray' name="7" />
                    <Button onClick={this.handleClick} color='lightgray' name="8" />
                    <Button onClick={this.handleClick} color='lightgray' name="9" />
                    <Button onClick={this.handleClick} name="x" />
                </div>
                <div style={this.rowStyle}>
                    <Button onClick={this.handleClick} color='lightgray' name="4" />
                    <Button onClick={this.handleClick} color='lightgray' name="5" />
                    <Button onClick={this.handleClick} color='lightgray' name="6" />
                    <Button onClick={this.handleClick} name="-" />
                </div>
                <div style={this.rowStyle}>
                    <Button onClick={this.handleClick} color='lightgray' name="1" />
                    <Button onClick={this.handleClick} color='lightgray' name="2" />
                    <Button onClick={this.handleClick} color='lightgray' name="3" />
                    <Button onClick={this.handleClick} name="+" />
                </div>
                <div style={this.rowStyle}>
                    <Button onClick={this.handleClick} color= 'lightgray' wide={true} name="0" />
                    <Button onClick={this.handleClick} color='lightgray' name="." />
                    <Button onClick={this.handleClick} name="=" />
                </div>
            </div>

        )
    }
}