import React from 'react';

export default class Button extends React.Component {
    constructor(props){
        super(props);

        this.handleClick = this.handleClick.bind(this);

        this.style = ((backgroundColor = this.props.color, wide = this.props.wide) => {
            const flexBasis = (wide) ? '50%' : '25%';
            return {
                backgroundColor: backgroundColor,
                flexBasis: flexBasis,
                border: '1px solid darkgray'
            }
        })();

    }

    handleClick = (buttonName) => { this.props.onClick(buttonName) };

    render() {
        return (
            <button onClick={() => { this.handleClick(this.props.name) }} style={this.style}>{this.props.name}</button>
        )
    }
}

Button.defaultProps = {
    color: 'orange',
    wide: false,
};